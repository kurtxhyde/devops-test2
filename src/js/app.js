import Vue from 'vue';

import '@/util/polyfill';
import App from '@/container/App';
import router from './pages/index/router';
import store from './pages/index/store';
// import {FB_ASSET} from '@/util/fb_assets';
import { config } from '@/util/config';
import utils from '@/util/utils';
//import utils from '@/util/
// import VueYoutube from 'vue-youtube'
// Vue.use(VueYoutube)
console.log('process.env.NODE_ENV', process.env.NODE_ENV);
console.log('process.env.ROOT', process.env.ROOT);
Vue.prototype.GLOBAL={TEST:false};
// utils.GA(config.GaId);
// Vue.prototype.GLOBAL.fb = new FB_ASSET(Vue.prototype.GLOBAL );


//Vue.prototype.GLOBAL.socket;
export default new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created:()=>{
    // this.function_that_has_scrollbehavior();
  },
  updated:()=>{
  	// this.function_that_has_scrollbehavior();
  }
});
