// console.log
import { TweenMax,Linear ,Sine , Expo,Back,Cubic} from 'gsap/all';
import { config } from './config';
 

export default class {
	
	static AnimBlur(OBJ , fromBLUR=0 , toBLUR=5 , time=1 , delay=0){
			let blurElement = OBJ
			let prop = { blur: fromBLUR }
			applyBlur( blurElement, prop)
			TweenMax.fromTo(prop, time ,{blur:fromBLUR},{delay:delay, blur: toBLUR, onUpdate: applyBlur, onUpdateParams: [			blurElement, prop] })
			//here you pass the filter to the DOM element
			function applyBlur(obj, prop) {
			// console.log(prop.blur)
			let a = prop.blur
			TweenMax.set(obj, { filter: "blur(" + a + "px)", webkitFilter: "blur(" + a + "px)" });
			}
	}
	// static AnimBlurX(OBJ , fromBLUR=0 , toBLUR=5 , time=1 , delay=0){
	// 		let blurElement = OBJ
	// 		let prop = { blur: fromBLUR }
	// 		applyBlur( blurElement, prop)
	// 		TweenMax.fromTo(prop, time ,{blur:fromBLUR},{delay:delay, blur: toBLUR, onUpdate: applyBlur, onUpdateParams: [			blurElement, prop] })
	// 		//here you pass the filter to the DOM element
	// 		function applyBlur(obj, prop) {
	// 		// console.log(prop.blur)
	// 		let a = prop.blur
	// 		TweenMax.set(obj, { filter: "blur(" + a + "px)", webkitFilter: "blur(" + a + "px)" });
	// 		}
	// }
}