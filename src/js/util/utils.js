// console.log

import { config } from './config';
import CryptoJS from 'crypto-js';
import device from "current-device";

export default class {
	static  getParameterByName(name, url = window.location.href) {
	const tname = name.replace(/[\[\]]/g, '\\$&');
	// const regex = new RegExp('[?&]' + tname + '(=([^&#]*)|&|#|$)');
	const regex = new RegExp(`[?&]${tname}(=([^&#]*)|&|#|$)`);
	const results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}



	
	static setCookie(cname,cvalue,exdays)
{
  var d = new Date();
  d.setTime(d.getTime()+(exdays*24*60*60*1000));
  var expires = "expires="+d.toGMTString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
}
	static getCookie(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) 
  {
    var c = ca[i].trim();
    if (c.indexOf(name)==0) return c.substring(name.length,c.length);
  }
  return "";
}
	static  random(max,min,blocked){
						let r  =Math.floor( Math.random()  * (max - min + 1)) + min;
						while(r == blocked){
							r  =Math.floor( Math.random()  * (max - min + 1)) + min;
						}
						return r;
	}
	static getBrowserWidth() {
	    let objectW;

	    if (document.body.clientWidth) {
	        objectW = document.body.clientWidth * 1;
	    }
	    if (document.documentElement.clientWidth) {
	        objectW = document.documentElement.clientWidth * 1;
	    }
	    if (window.innerWidth) {
	        objectW = window.innerWidth * 1;
	    }
	    return objectW;
	}

	static chkIEver(){
		function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    
        var ua = navigator.userAgent;
        var re = ua.indexOf("MSIE ");
        if (ua.indexOf("MSIE ") >=0 || ua.indexOf("Trident/") >=0 || ua.indexOf("Edge/") >=0 )
        {
        	rv = "IE"
        };
    	console.log("rv:" + rv)
    return rv;
}
	function 		checkVersion() {
    var msg = "You’re not using Windows Internet Explorer.";
    var ver = getInternetExplorerVersion();
    if (ver > -1) {
        if (ver >= 9.0)
            msg = "Internet Explorer >= 9";
        else
            msg = "ie8"
    }
    		return ver;
		}
		return checkVersion();
	}
	
	static  getBrowserHeight() {
	let objectH;
	
	if (document.body.clientHeight) {
		objectH = document.body.clientHeight * 1;
	}
	if (document.documentElement.clientHeight) {
		objectH = document.documentElement.clientHeight * 1;
	}
	if (window.innerHeight) {
		objectH = window.innerHeight * 1;
	}
	return objectH;
	}

	static startLoading(pElemId, pCallback){
		var imgLoad  = imagesLoaded( pElemId );
		var _total   = imgLoad.images.length;
		var _current = 0;
		imgLoad.on( 'progress', function( instance, image ) {
			_current++;
			var _percentage = Math.floor((_current/_total)*100);
		});
		
		imgLoad.on( 'always', function( instance, image ) {
			if (typeof pCallback === 'function')pCallback();
		});
	}

	static gaEvent(category, label) {
		if (!gtag)throw 'gtag not ready';
		else {
			gtag('event', 'click', {
				'event_category':category,
				'event_label'   :label
			});
			console.log('gaEvent: /'+category+'/'+label);
		}
	}

	static gaPageview(title, path) {
		if (!gtag)throw 'gtag not ready';
		else {
			gtag('config', config.gaId, {
				'page_title':title,
				'page_path' :path
			});
			console.log('gaPageview: /'+title+'/'+path);
		}
	}

	static cryptoUserID(userID) {
		let uid = CryptoJS.TripleDES.encrypt(userID, CryptoJS.enc.Utf8.parse(config.key), {mode: CryptoJS.mode.ECB}).toString();
		return uid;
	}

	static ellipsis(){
		$('.ellipsis').each(function(i){
			let len = $(this).attr('data-len');
			if($(this).text().length>len){
				$(this).attr('title',$(this).text());
				let txt=$(this).text().substring(0,len-1)+'...';
				$(this).text(txt);
			}
	   });
	}

	static isScrolledIntoView(elem)
	{
    	var docViewTop = $(window).scrollTop();
    	var docViewBottom = docViewTop + $(window).height();
    	var elemTop = elem.offset().top;
    	var elemBottom = elemTop + elem.height();
    	return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	}
	static fixfloatIssue(num, deci=2){
		let k =1
		for(let i = 0; i < deci ;i++){
			k *= 10;
		}
		let Integer = Math.round(num * k) ;
		return Integer/k;
	}
	static gTagConvertionLabel(CID, CLABEL ){
		console.log(`gtag conversion:...AW-${CID}/${CLABEL}`)
		gtag('event', 'conversion', {'send_to': `AW-${CID}/${CLABEL}`,'transaction_id': ''});
	}
	///
	static gTagEvent(category, action, label=""){
		console.log(`ga id:...${category} /${action} /${label}`);
		
		try{
			function gtag(){dataLayer.push(arguments);}
 			gtag('event', action, {
 		    'event_category': category,
 		    'event_label': label,
 		    'value': ''
 			});
		}catch(err){
			
		}
		
 	}


 	static GtagCfg(ID ,page_path ,newpagehtml ){
 		function gtag(){dataLayer.push(arguments);}
 		// if(device.desktop()){
 		//   	if(page_path.indexOf('m_')>=0){
 		//   		page_path = page_path.replace('m_' , 'pc_')
 		//   	}
 		//   	if(newpagehtml.indexOf('m_')>=0){
 		//   		newpagehtml = newpagehtml.replace('m_' , 'pc_')
 		//   	}
 		// }



 		console.log(`ga id:${ID}....page:${newpagehtml}`)
 		try{
 			gtag('config', ID, {page_path:newpagehtml});
 		}catch(err){}
 		
 	}

	static GA(ID){
 		window.dataLayer = window.dataLayer || [];
 		function gtag(){dataLayer.push(arguments);}
 		gtag('js', new Date());
 		gtag('config', ID);
 		console.log(`gtag id:${ID}....page view`)
	}
	
}