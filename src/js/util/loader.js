
class LoadModle {
	constructor(imgs , cb = null){
		this.$imgs = imgs
		this.imagesN = imgs.length;
		this.cb = cb;
		this.c = 0;
		this.count = 0;
		this.oldInd = 0;
		this.init();
	}
	reset(){
	}
	anim(typ) {
		// console.log('anim...............');
	}
	pngFix() {
		const self = $(this);
		self.find('img[src$=".png"],img[src$=".gif"]').each(() => {
			console.log('png Fix!....')
			this.style.filter =
			`progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='image',src='${this.src}')`;
		});
	}
	init() {

		let self = this;
		//preventScroll()
		self.reset();

		if(self.$imgs.length==0){
			if(self.cb){self.cb();}
			return;
		}
		

		self.$imgs.imageready(function(){
			self.count++;
			console.log(`loaded...${self.$imgs.length}`);
			let per = Math.floor((self.count / self.$imgs.length) * 100) ;
			let txt = $('.loading .ld .txt');
			txt.html(per)

			
			self.oldInd = self.ind;
			if (self.count === self.$imgs.length) {
				self.pngFix();
				if(self.cb){
					self.cb();
				}
				
			}
		})

	};
	
}

export {
	LoadModle
}