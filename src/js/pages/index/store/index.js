// @flow
import Vuex from 'vuex';
import Vue from 'vue';

export type State = {
   
  loading: boolean,
  bodyTop :number,
  loaded:number,
  sections:number,
  loadingajax :boolean,
  hinting:boolean,
  confirming:boolean,
  hintMsg:string,
  end: boolean,
  gotoLevel:number,
  justbreak:number,
  justfrom:number,

  PoseHintMsg:string,
  PoseHinting:boolean,
  popkol: boolean,
  playing :number,
}
Vue.use(Vuex);
const defaultStat:State = {
  //'AIzaSyD0vCZeoCYmA5kgbM6p_pa5_ecuGz1_704',confirming
  loading: true,
  loaded:0,
  bodyTop:0 ,

  sections:6,
  loadingajax:false,
  hinting:false,
  confirming: false,
  hintMsg:'請按照順序挑戰關卡哦',
  end:false,
  gotoLevel:1,
  justbreak:0,
  justfrom:0,
  PoseHintMsg:'',
  PoseHinting:false,

  popkol:false,
  playing :0,
};
// vue 裡用 this.$store.commit('loading' , true)
const mutations = {
  
  loading(state:State, value:boolean) {
    state.loading = value;
  },
  loadingajax(state:State, value:boolean) {
    state.loadingajax = value;
  },
  loaded(state:State, value:boolean) {
    state.loaded = value;
  },
  bodyTop(state:State, value:number) {
    state.bodyTop = value;
  },
  hinting(state:State, value:boolean) {
    state.hinting = value;
  },
  popkol(state:State, value:boolean){
    state.popkol = value;
  },

  PoseHinting(state:State, value:boolean){  
    state.PoseHinting = value;
  },
  confirming(state:State, value:boolean) {
    state.confirming = value;
  },
  hintMsg(state:State, value:string) {
    state.hintMsg = value;
  },
  end(state:State, value:boolean){
    state.end = value;
  },

  gotoLevel(state:State, value:number){
    state.level = value;
  },

   playing(state:State, value:number) {
    state.playing = value;
  },
};
/*
  vue 裡用 this.$store.dispatch('loading' , true)
  methods(){
    ...Vuex.mapActions(['loading']),
  }
*/
const actions = {
   
  loading({ commit }, value:boolean) {
    commit('loading', value);
  },
  loadingajax({ commit }, value:boolean) {
    commit('loadingajax', value);
  },
  loaded({ commit }, value:boolean) {
    commit('loaded', value);
  },
  bodyTop({ commit }, value:number) {
    commit('bodyTop', value);
  },
  hinting({ commit }, value:boolean) {
    commit('hinting', value);
  },
  PoseHinting({ commit }, value:boolean){
    commit('PoseHinting', value);
  },
  confirming({ commit }, value:boolean) {
    commit('confirming', value);
  },
  popkol({commit} ,   value:boolean){
    commit( 'popkol' ,value);
  },


  end({commit}, value:boolean){
    commit('end', value);
  },
   playing({ commit }, value:number) {
    commit('playing', value);
  },
  gotoLevel({commit}, value:number){
    commit('gotoLevel', value);
  }
};
/**
  computed:{
    ...Vuex.mapGetters(['loading'])
  },
*/
const getters = {
   
  loading: ({ loading }) => loading,
  loaded: ({ loaded }) => loaded,
  bodyTop: ({ bodyTop }) => bodyTop,
  sections: ({ sections }) => sections,
  loadingajax:({loadingajax}) => loadingajax,
  
  hinting:({hinting}) => hinting,
  PoseHinting: ({ PoseHinting }) => PoseHinting,
  playing: ({ playing }) => playing,
  confirming: ({ confirming }) => confirming,
  end:({end}) => end,
  gotoLevel:({ gotoLevel}) => gotoLevel,
  justbreak:({justbreak}) => justbreak,
  justfrom:({ justfrom}) => justfrom,

  popkol:({popkol}) => popkol,
};
export default new Vuex.Store({
  state: defaultStat,
  getters,
  actions,
  mutations,

});
