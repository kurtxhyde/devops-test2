import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';
import {jquery} from 'jquery';
import { config } from '@/util/config';
import utils from '@/util/utils';
let $ = require('jquery');
const Main = () => import('@/component/Main');
const Intro = () => import('@/component/Intro');
const Buy = () => import('@/component/Buy');
const Choice = () => import('@/component/Choice');

const Game = () => import('@/component/Game');
const Detail = () => import('@/component/Detail');
const Gallery = () => import('@/component/Gallery');
const Custom = () => import('@/component/Custom');
// const Coupons = () => import('@/component/Coupons');
// const test = () => import('@/component/popForm');
// const About = () => import(/* webpackChunkName: "About" */'@/component/About');

Vue.use(VueRouter);

export const routes:Array = [
  { path: '/', component: Main ,name:'index'},
  { path: '/index', component: Main ,name:'index'},
  { path: '/intro', component: Intro ,name:'intro'},
  { path: '/choice', component: Choice ,name:'choice'},
  { path: '/game', component: Game ,name:'game'},
  { path: '/detail', component: Detail ,name:'detail'},
  { path: '/gallery', component: Gallery ,name:'gallery'},
  { path: '/custom', component: Custom ,name:'custom'},
  { path: '/buy', component: Buy ,name:'buy'},
  // { path: '/exchange', component: Exchange ,name:'exchange'},
];
const DEV_MODE = (process.env.NODE_ENV === 'development');

const ROOT = process.env.ROOT//'/EssentialEnergy/dev/'
const  bodyscrollTo = (top)=>{
    let $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
    const speed = 5;
    let t = 500;
    console.log(`scroll to.... ${top}`)
    $body.animate({
        scrollTop: top
    }, t );
    
}
const router = new VueRouter({
  mode: 'hash',
  routes,
  scrollBehavior (to, from, savedPosition) {
  },
});
router.beforeEach((to, from, next) => {
  
  switch(to.params.typ){
    default:
      if(typeof to.params.typ != 'undefined'){
        // GLOBAL.ga.GT( '/' + to.params.typ  , '.btn.' + to.name )
        
      }
      break;
  }

  if (to.matched.some(record => record.meta.authorization || false)) {
    const isLogin = store.state.isLogin;
    if (isLogin) {
      next();
    } else {
      next({ path: '/login', query: { redirect: to.fullPath } });
    }
  } else {
    next();
  }
});
router.afterEach((to, from) => {
    console.log(`to.....${to.name}`)
    Vue.nextTick(() => {
      switch( to.name){
          case 'custom':
          {
            utils.GtagCfg(config.GaIdUA, `./#/${to.name}`, `./start.html` )
          }
          break;
          case 'gallery':
          {
            utils.GtagCfg(config.GaIdUA, `./#/${to.name}`, `./createpose.html` )
          }
          break;
          case 'game':
          {
            utils.GtagCfg(config.GaIdUA, `./#/${to.name}`, `./18poses.html` )
          }
          break;
          case 'buy':
          {
            utils.GtagCfg(config.GaIdUA, `./#/${to.name}`, `./buy.html` )
          }
          break;
          case 'choice':
          {
            utils.GtagCfg(config.GaIdUA, `./#/${to.name}`, `./singlebedbed.html` )
          }
          break;
          case 'intro':
          {
            utils.GtagCfg(config.GaIdUA, `./#/${to.name}`, `./intro.html` )
          }
          break;
          case 'index':
          {
            // utils.GtagCfg(config.GaIdUA, `./#/${to.name}`, `./agree.html` )
          }
          break;
          default:
            {
              bodyscrollTo(0);
            }
          break
    }
    })
})
export default router;
